<?php
/**
 * The template for displaying the footer.
 *
 * Contains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
 ?>

				<footer class="footer" role="contentinfo">
          <div class="row">
            <!-- show on mobile only -->
            <div class="small-12 columns show-for-small-only text-center">
              <a href="#" class="btn btn-white btn-newsletter btn-newsletter--solid">Get our newsletter</a>
              <ul class="social-icons">
                <li>
                  <a href="https://www.facebook.com/pages/Monterey-Bay-Economic-Partnership/612141838881488">
                    <span class="social-icon">
                      <i class="fab fa-facebook-f fa-2x"></i>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="https://twitter.com/MBEPartnership">
                    <span class="social-icon">
                      <i class="fab fa-twitter fa-2x"></i>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#Instagram">
                    <span class="social-icon">
                      <i class="fab fa-instagram fa-2x"></i>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/company/monterey-bay-economic-partnership/">
                    <span class="social-icon">
                      <i class="fab fa-linkedin-in fa-2x"></i>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
            <!-- end of show on mobile only -->
            <div class="small-12 medium-2 columns text-center medium-text-left">
              <h3>Explore</h3>
              <ul>
                <li><a href="#">Who we are</a></li>
                <li><a href="#">What we do</a></li>
                <li><a href="#">Members</a></li>
                <li><a href="#">Data Insights</a></li>
                <li><a href="#">Events</a></li>
              </ul>
            </div>
            <div class="small-12 medium-2 columns text-center medium-text-left">
              <h3>Initiatives</h3>
              <ul>
                <li><a href="#">Transportation</a></li>
                <li><a href="#">Housing</a></li>
                <li><a href="#">Workforce</a></li>
                <li><a href="#">Technology</a></li>
              </ul>
            </div>
            <div class="small-12 medium-6 large-4 columns hide-for-small-only text-center">
              <a href="#" class="btn btn-white btn-newsletter btn-newsletter--solid">Get our newsletter</a>
              <ul class="social-icons">
                <li>
                  <a href="https://www.facebook.com/pages/Monterey-Bay-Economic-Partnership/612141838881488">
                    <span class="social-icon">
                      <i class="fab fa-facebook-f fa-2x"></i>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="https://twitter.com/MBEPartnership">
                    <span class="social-icon">
                      <i class="fab fa-twitter fa-2x"></i>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="#Instagram">
                    <span class="social-icon">
                      <i class="fab fa-instagram fa-2x"></i>
                    </span>
                  </a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/company/monterey-bay-economic-partnership/">
                    <span class="social-icon">
                      <i class="fab fa-linkedin-in fa-2x"></i>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
            <div class="small-12 medium-2 large-offset-2 columns end text-center medium-text-left">
              <h3>Contact Us</h3>
              <ul>
                <li>
                  <address>
                    3180 Imjin Road, Suite 104B<br>
                    Marina, CA 93933
                  </address>
                </li>
                <li><a href="tel:1-831-915-2806">831.915.2806</a></li>
                <li><a href="mailto:admin@mbep.biz">admin@mbep.biz</a></li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="small-12 columns text-center copyright">
              <p>Copyright &copy; <?php echo date('Y'); ?> Monterey Bay Economic Partnership.</p>
              <p>All rights reserved. </p>
              <p><span class="hide-for-small-only">|</span> Design by <a href="https://www.sleeplessmedia.com/">Sleepless Media</a></p>
            </div>
          </div>
				</footer> <!-- end .footer -->
		  <!-- end .off-canvas-content WAS HERE -->
      <!-- end .off-canvas-wrapper WAS HERE -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->
