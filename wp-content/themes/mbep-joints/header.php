<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,400i,700i,900i|Muli:400,700,800" rel="stylesheet">
    <!-- Font Awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
	    <?php } ?>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

    <div class="mobile-menu-overlay"></div>

    <!-- <div class="off-canvas-wrapper"> -->

      <header class="main-header" role="banner">

        <a href="/" class="logo">
          <img src="http://via.placeholder.com/233x76" alt="Monterey Bay Economic Partnership logo">
        </a>

        <ul class="nav">
          <li class="link-desktop"><a href="/who-we-are">Who We Are</a></li>
          <li class="link-desktop"><a href="/what-we-do">What We Do</a></li>
          <li class="link-desktop"><a href="/members">Members</a></li>

          <!-- Search Bar Experiment -->
          <li>

            <div class="search-wrapper">
              <div class="search-bar">
                <input type="text" />
              </div>
              <div class="search-test">
                <?php get_template_part( 'assets/images/icons/homepage/header/icon', 'search.svg'); ?>
              </div>
            </div>

          </li>
          <!-- END - Search Bar Experiment -->

          <li class="search">
            <?php get_template_part( 'assets/images/icons/homepage/header/icon', 'search.svg'); ?>
          </li>
          <li class="hamburger">
            <?php get_template_part( 'assets/images/icons/homepage/header/icon', 'hamburger.svg'); ?>
          </li>
        </ul>

				 <?php get_template_part( 'parts/nav', 'aside' ); ?>

			</header> <!-- end .header -->
