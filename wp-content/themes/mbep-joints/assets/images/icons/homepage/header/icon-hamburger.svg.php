
<svg width="33px" height="19px" viewBox="0 0 33 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 49.1 (51147) - http://www.bohemiancoding.com/sketch -->
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Home-Page---FINAL" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Tablet" transform="translate(-705.000000, -37.000000)" fill="#B3B2B3">
            <g id="Group-Copy" transform="translate(705.000000, 37.000000)">
                <polygon id="Rectangle-5" points="0 16 30 16 33 19 3 19"></polygon>
                <polygon id="Rectangle-5-Copy" points="0 8 23.6363636 8 26 11 2.36363636 11"></polygon>
                <polygon id="Rectangle-5-Copy-2" points="0 0 30 0 33 3 3 3"></polygon>
            </g>
        </g>
    </g>
</svg>
