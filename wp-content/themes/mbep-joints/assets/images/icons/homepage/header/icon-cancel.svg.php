
<svg width="19px" height="19px" viewBox="0 0 19 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 49.1 (51147) - http://www.bohemiancoding.com/sketch -->
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Home-Page---FINAL" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Tablet_Menu" transform="translate(-710.000000, -34.000000)" fill="#B3B2B3">
            <polygon id="Page-1" points="712.110849 34 710 36.1109817 717.389151 43.5005971 710 50.8890183 712.110849 53 719.5 45.6115789 726.889151 53 729 50.8890183 721.610849 43.5005971 729 36.1109817 726.889151 34 719.5 41.3896154"></polygon>
        </g>
    </g>
</svg>
