jQuery(document).foundation();

jQuery(document).ready(function() {

  // Header - toggle aside
  var hamburger = document.querySelector('.hamburger');
  var cancel = document.querySelector('.cancel');
  var sidebar = document.querySelector('.home .sidebar');
  var mobileMenuOverlay = document.querySelector('.mobile-menu-overlay');

  hamburger.addEventListener('click', function() {
    sidebar.classList.toggle('active');
    mobileMenuOverlay.classList.toggle('is-active');
  });

  cancel.addEventListener('click', function() {
    sidebar.classList.toggle('active');
    mobileMenuOverlay.classList.toggle('is-active');
  });

  // Homepage Category Icon hover - is this working?
  jQuery('.home .icon').hover(
    function() {
      jQuery(this).addClass('hover');
    }, function() {
      jQuery(this).removeClass('hover');
    }
  );


  // Search Bar Experiment
  jQuery('.search-test').click(function(){
  jQuery('.search-test, .search-bar').toggleClass('active');
  jQuery('input').focus();
});




}); // end .ready
