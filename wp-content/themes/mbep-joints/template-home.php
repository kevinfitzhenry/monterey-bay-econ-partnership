<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>

<?php

  $video = get_field('homepage_hero_video_file');

  if($video) { ?>

    <section class="hero hero-video">
      <div class="hero-video__overlay"></div>
      <h1 class="text-white"><?php the_field('homepage_hero_text') ?></h1>
      <a href="<?php the_field('homepage_hero_cta_button_link') ?>" class="btn btn-red"><?php the_field('homepage_hero_cta_button_text') ?></a>
      <div class="flex-video widescreen">
        <video width="100%" height="100%" preload="auto" autoplay muted loop>
          <source src="<?php the_field('homepage_hero_video_file') ?>" type="video/mp4">
        </video>
      </div>
    </section>

<?php

  } else {

  $image = get_field('homepage_hero_image');

  if( !empty($image) ): ?>

    <section class="hero hero-image" style="background-image: url('<?php the_field('homepage_hero_image') ?>')">
      <div class="row center">
        <div class="small-12 columns text-center">
          <h1 class="text-white"><?php the_field('homepage_hero_text') ?></h1>
          <a href="<?php the_field('homepage_hero_cta_button_link') ?>" class="btn btn-red"><?php the_field('homepage_hero_cta_button_text') ?></a>
        </div>
      </div>
    </section>

<?php endif;
  }
?>

<section class="icons">
  <div class="row fullWidth collapse">
    <div class="small-12 large-6 columns">
      <div class="row collapse">
        <div class="small-4 columns icon">
          <a href="#">
            <div class="icon__content">
              <div class="icon__graphic">
                <?php get_template_part( 'assets/images/icons/homepage/categories/icon', 'events.svg'); ?>
              </div>
              <p>Events</p>
            </div>
          </a>
        </div>
        <div class="small-4 columns icon">
          <a href="#">
            <div class="icon__content">
              <div class="icon__graphic">
                <?php get_template_part( 'assets/images/icons/homepage/categories/icon', 'transportation.svg'); ?>
              </div>
              <p>Transportation</p>
            </div>
          </a>
        </div>
        <div class="small-4 columns icon end">
          <a href="#">
            <div class="icon__content">
              <div class="icon__graphic">
                <?php get_template_part( 'assets/images/icons/homepage/categories/icon', 'housing.svg'); ?>
              </div>
              <p>Housing</p>
            </div>
          </a>
        </div>
      </div>
    </div>
    <div class="small-12 large-6 columns collapse end">
      <div class="row collapse">
        <div class="small-4 columns icon">
          <a href="#">
            <div class="icon__content">
              <div class="icon__graphic">
                <?php get_template_part( 'assets/images/icons/homepage/categories/icon', 'workforce.svg'); ?>
              </div>
              <p>Workforce</p>
            </div>
          </a>
        </div>
        <div class="small-4 columns icon">
          <a href="#">
            <div class="icon__content">
              <div class="icon__graphic">
                <?php get_template_part( 'assets/images/icons/homepage/categories/icon', 'technology.svg'); ?>
              </div>
              <p>Technology</p>
            </div>
          </a>
        </div>
        <div class="small-4 columns icon end">
          <a href="#">
            <div class="icon__content">
              <div class="icon__graphic">
                <?php get_template_part( 'assets/images/icons/homepage/categories/icon', 'data-insights.svg'); ?>
              </div>
              <p>Data Insights</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="news-and-stories">

  <div class="row">
    <div class="small-12 columns">
      <h1 class="headline text-center"><?php the_field('homepage_block_grid_header') ?></h1>
    </div>
  </div>

  <?php
    if( have_rows('homepage_block_grid') ):
      while ( have_rows('homepage_block_grid') ) : the_row();
        // Get icon
        $icon = get_sub_field('homepage_block_grid_icon') . '.svg';
        // If Even Row
        if (0 == $i % 2) {
  ?>

  <div class="row">
    <div class="small-12 medium-6 medium-push-6 columns end block block-image" style="background-image: url('<?php the_sub_field('homepage_block_grid_image') ?>');"></div>
    <div class="small-12 medium-6 medium-pull-6 columns end block block-post block-post__even <?php the_sub_field('homepage_block_grid_color') ?>">
      <div class="icon">
        <?php get_template_part( 'assets/images/icons/homepage/blocks/icon', $icon); ?>
      </div>
      <div class="block-post__content text-center">
        <p class="block-post__date"><?php the_sub_field('homepage_block_grid_post_subtitle') ?></p>
        <h4 class="block-post__title"><?php the_sub_field('homepage_block_grid_post_title') ?></h4>
        <a href="<?php the_sub_field('homepage_block_grid_link') ?>" class="btn btn-dark"><?php the_sub_field('homepage_block_grid_post_cta') ?></a>
      </div>
    </div>
  </div>

  <?php
    // If Odd Row
    } else {
  ?>

  <div class="row hide-for-small-only">
    <div class="small-12 medium-6 medium-push-6 columns block block-post block-post__odd <?php the_sub_field('homepage_block_grid_color') ?>">
      <div class="icon">
        <?php get_template_part( 'assets/images/icons/homepage/blocks/icon', $icon); ?>
      </div>
      <div class="block-post__content text-center">
        <p class="block-post__date"><?php the_sub_field('homepage_block_grid_post_subtitle') ?></p>
        <h4 class="block-post__title"><?php the_sub_field('homepage_block_grid_post_title') ?></h4>
        <a href="<?php the_sub_field('homepage_block_grid_link') ?>" class="btn btn-dark"><?php the_sub_field('homepage_block_grid_post_cta') ?></a>
      </div>
    </div>
    <div class="small-12 medium-6 medium-pull-6 columns end block block-image" style="background-image: url('<?php the_sub_field('homepage_block_grid_image') ?>');"></div>
  </div>

  <div class="row show-for-small-only">
    <div class="small-12 columns end block block-image" style="background-image: url('<?php the_sub_field('homepage_block_grid_image') ?>');"></div>
    <div class="small-12 columns end block block-post block-post__even <?php the_sub_field('homepage_block_grid_color') ?>">
      <div class="icon">
        <?php get_template_part( 'assets/images/icons/homepage/blocks/icon', $icon); ?>
      </div>
      <div class="block-post__content text-center">
        <p class="block-post__date"><?php the_sub_field('homepage_block_grid_post_subtitle') ?></p>
        <h4 class="block-post__title"><?php the_sub_field('homepage_block_grid_post_title') ?></h4>
        <a href="<?php the_sub_field('homepage_block_grid_link') ?>" class="btn btn-dark"><?php the_sub_field('homepage_block_grid_post_cta') ?></a>
      </div>
    </div>
  </div>

  <?php } $i++; endwhile; endif; ?>

  <div class="row">
    <div class="small-12 columns text-center explore">
      <p class="text-center">
        <!-- link to all posts? -->
        <a href="#"><?php the_field('homepage_posts_cta') ?></a>
      </p>
    </div>
  </div>

</section>

<section class="newsletter-sign-up">
  <div class="row fullWidth">
    <div class="small-12 columns text-center">
      <a href="#" class="btn btn-white btn-newsletter btn-newsletter--clear center">Get our newsletter</a>
    </div>
  </div>
</section>

<section class="quote">
  <div class="row">
    <div class="small-12 columns text-center">
      <div class="content">
        <img class="quotation quotation-left" src="<?php bloginfo('template_url'); ?>/assets/images/homepage/quotation-mark.png" alt="quotation mark icon">
        <img class="quotation quotation-right" src="<?php bloginfo('template_url'); ?>/assets/images/homepage/quotation-mark.png" alt="quotation mark icon">
        <h2 class="text-center text-blue"><?php the_field('homepage_quote') ?></h2>
        <p class="text-center"><?php the_field('homepage_quote_person') ?></p>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
