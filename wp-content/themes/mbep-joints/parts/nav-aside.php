<div class="sidebar">
  <div class="section section__top">
    <div class="search v-center" style="display:none;">
      <?php get_template_part( 'assets/images/icons/homepage/header/icon', 'search.svg'); ?>
    </div>
    <div class="cancel v-center">
      <?php get_template_part( 'assets/images/icons/homepage/header/icon', 'cancel.svg'); ?>
    </div>
  </div>
  <nav>
    <div class="section section__middle">
      <ul>
        <h3>Initiatives</h3>
        <li><a href="#">Transportation</a></li>
        <li><a href="#">Housing</a></li>
        <li><a href="#">Workforce</a></li>
        <li><a href="#">Technology</a></li>
      </ul>
    </div>
    <div class="section section__bottom">
      <ul>
        <h3>Explore</h3>
        <li><a href="#">Who We Are</a></li>
        <li><a href="#">What We Do</a></li>
        <li><a href="#">Members</a></li>
        <li><a href="#">Events</a></li>
        <li><a href="#">Data Insights</a></li>
        <li><a href="#">Contact Us</a></li>
      </ul>
    </div>
  </nav>
</div>
